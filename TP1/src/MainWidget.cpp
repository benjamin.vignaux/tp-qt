/**
 * @file   MainWidget.cpp
 * @author Sebastien Fourey
 *
 * @brief  MainWidget methods definitions.
 */
#include "MainWidget.h"
#include <QMessageBox>
#include <QMouseEvent>
#include <QSlider>
#include <QSpinBox>
#include <QString>
using namespace std;

MainWidget::MainWidget()
{
  setGeometry(100, 100, 640, 480);

  QLabel * label = new QLabel("Colors, signals, and slots", this);
  label->setGeometry(10, 10, 290, 30);
  label->setFont(QFont("Arial", 14, QFont::Bold));

  QSpinBox * spinBox = new QSpinBox(this); // Just for testing the -style option
  spinBox->setGeometry(300, 10, 80, 40);   // Just for testing the -style option

  QSlider * slider = new QSlider(this);   // Just for testing the -style option
  slider->setOrientation(Qt::Horizontal); // Just for testing the -style option
  slider->setGeometry(400, 10, 150, 50);  // Just for testing the -style option

  lineDisplay = new QLineEdit("", this);
  lineDisplay->setGeometry(250, 70, 200, 30);
  lineDisplay->setAlignment(Qt::AlignHCenter);

  ColorWidget * colorWidget = new ColorWidget(this);
  colorWidget->setGeometry(300, 160, 80, 30);

  QPushButton * pushButtonRandomColor = new QPushButton("Random color", this);
  pushButtonRandomColor->setGeometry(260, 200, 160, 30);

  QPushButton * pushButtonQuit = new QPushButton("Quit", this);
  pushButtonQuit->setGeometry(300, 250, 80, 30);

  connect(pushButtonQuit, SIGNAL(clicked()), this, SLOT(quitPressed()));

  connect(pushButtonRandomColor, SIGNAL(clicked()), colorWidget, SLOT(changeColor()));

  connect(colorWidget, SIGNAL(colorChanged(int, int, int)), this, SLOT(colorChanged(int, int, int)));
}

void MainWidget::quitPressed()
{
  QMessageBox::StandardButton button;
  button = QMessageBox::question(this, "You want to quit...",
                                 "Are you sure that you want to quit"
                                 " this great application?",
                                 QMessageBox::Yes | QMessageBox::No, QMessageBox::No);
  if (button == QMessageBox::Yes) {
    close();
  }
}

void MainWidget::colorChanged(int r, int g, int b)
{
  QString text("Color: R(%1) G(%2) B(%3)");
  lineDisplay->setText(text.arg(r).arg(g).arg(b));
}
