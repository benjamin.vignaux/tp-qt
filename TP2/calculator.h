#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLineEdit>
#include <QComboBox>
#include "Controller.h"

class Calculator : public QMainWindow
{
    Q_OBJECT

public:
    Calculator();

public slots:
    void showHelp();
    void left();
    void buttonClick(int id);

private:
    QWidget * centralWidget;
    QLineEdit * input;
    QComboBox * box;
    Controller * controller;
};
#endif // MAINWINDOW_H
