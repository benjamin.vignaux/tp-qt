#include "calculator.h"
#include <QMenuBar>
#include <QMenu>
#include <QMessageBox>
#include <QLayout>
#include <QLabel>
#include <QPushButton>
#include <QButtonGroup>
#include "Controller.h"

Calculator::Calculator()
{
    setWindowTitle("Calculatrice");

    controller = new Controller;

    QMenu * fileMenu = menuBar()->addMenu("&Fichier");
    QAction * left = new QAction("&Quitter", this);
    fileMenu->addAction(left);
    connect(left, &QAction::triggered, this, &Calculator::left);

    QMenu * helpMenu = menuBar()->addMenu("&Aide");
    QAction * help = new QAction("&A propos", this);
    helpMenu->addAction(help);
    connect(help, &QAction::triggered, this, &Calculator::showHelp);

    centralWidget = new QWidget(this);

    QVBoxLayout * vBox = new QVBoxLayout(centralWidget);
    QLabel * title = new QLabel("Calculatrice", centralWidget);
    vBox->addWidget(title);

    input = new QLineEdit(centralWidget);
    input->setAlignment(Qt::AlignRight);
    vBox->addWidget(input);

    QGridLayout * grid = new QGridLayout;
    vBox->addLayout(grid);

    QPushButton * button0 = new QPushButton("0", centralWidget);
    QPushButton * button1 = new QPushButton("1", centralWidget);
    QPushButton * button2 = new QPushButton("2", centralWidget);
    QPushButton * button3 = new QPushButton("3", centralWidget);
    QPushButton * button4 = new QPushButton("4", centralWidget);
    QPushButton * button5 = new QPushButton("5", centralWidget);
    QPushButton * button6 = new QPushButton("6", centralWidget);
    QPushButton * button7 = new QPushButton("7", centralWidget);
    QPushButton * button8 = new QPushButton("8", centralWidget);
    QPushButton * button9 = new QPushButton("9", centralWidget);

    QPushButton * buttonA = new QPushButton("A", centralWidget);
    QPushButton * buttonB = new QPushButton("B", centralWidget);
    QPushButton * buttonC = new QPushButton("C", centralWidget);
    QPushButton * buttonD = new QPushButton("D", centralWidget);
    QPushButton * buttonE = new QPushButton("E", centralWidget);
    QPushButton * buttonF = new QPushButton("F", centralWidget);

    QPushButton * buttonPlus = new QPushButton("+", centralWidget);
    QPushButton * buttonMinus = new QPushButton("-", centralWidget);
    QPushButton * buttonDiv = new QPushButton("/", centralWidget);
    QPushButton * buttonMul = new QPushButton("*", centralWidget);
    QPushButton * buttonEqual = new QPushButton("=", centralWidget);
    QPushButton * buttonPoint = new QPushButton(",", centralWidget);

    QPushButton * buttonReset = new QPushButton("C", centralWidget);
    QPushButton * buttonQuit = new QPushButton("Quit", centralWidget);

    QButtonGroup * buttonGroup = new QButtonGroup(centralWidget);
    buttonGroup->addButton(button0, 0);
    buttonGroup->addButton(button1, 1);
    buttonGroup->addButton(button2, 2);
    buttonGroup->addButton(button3, 3);
    buttonGroup->addButton(button4, 4);
    buttonGroup->addButton(button5, 5);
    buttonGroup->addButton(button6, 6);
    buttonGroup->addButton(button7, 7);
    buttonGroup->addButton(button8, 8);
    buttonGroup->addButton(button9, 9);
    buttonGroup->addButton(buttonA, 10);
    buttonGroup->addButton(buttonB, 11);
    buttonGroup->addButton(buttonC, 12);
    buttonGroup->addButton(buttonD, 13);
    buttonGroup->addButton(buttonE, 14);
    buttonGroup->addButton(buttonF, 15);
    buttonGroup->addButton(buttonPoint, 16);
    buttonGroup->addButton(buttonPlus, 17);
    buttonGroup->addButton(buttonMinus, 18);
    buttonGroup->addButton(buttonDiv, 19);
    buttonGroup->addButton(buttonMul, 20);
    buttonGroup->addButton(buttonEqual, 21);
    buttonGroup->addButton(buttonReset, 22);
    buttonGroup->addButton(buttonQuit, 23);

    connect(buttonGroup, SIGNAL(buttonClicked(int)), this, SLOT(buttonClick(int)));

    QLabel * base = new QLabel("Base", centralWidget);
    base->setAlignment(Qt::AlignRight);

    box = new QComboBox(centralWidget);
    box->addItems({"Dec", "Bin", "Hex"});

    grid->addWidget(button0, 5, 0);
    grid->addWidget(button1, 4, 0);
    grid->addWidget(button2, 4, 1);
    grid->addWidget(button3, 4, 2);
    grid->addWidget(button4, 3, 0);
    grid->addWidget(button5, 3, 1);
    grid->addWidget(button6, 3, 2);
    grid->addWidget(button7, 2, 0);
    grid->addWidget(button8, 2, 1);
    grid->addWidget(button9, 2, 2);

    grid->addWidget(buttonA, 1, 0);
    grid->addWidget(buttonB, 1, 1);
    grid->addWidget(buttonC, 1, 2);
    grid->addWidget(buttonD, 1, 3);
    grid->addWidget(buttonE, 0, 0);
    grid->addWidget(buttonF, 0, 1);

    grid->addWidget(buttonPlus, 2, 3);
    grid->addWidget(buttonMinus, 3, 3);
    grid->addWidget(buttonDiv, 4, 3);
    grid->addWidget(buttonMul, 5, 3);
    grid->addWidget(buttonEqual, 5, 2);
    grid->addWidget(buttonPoint, 5, 1);

    grid->addWidget(buttonReset, 6, 0, 1, 4);
    grid->addWidget(buttonQuit, 7, 0, 1, 4);

    grid->addWidget(base, 0, 2);
    grid->addWidget(box, 0, 3);

    this->setCentralWidget(centralWidget);
}

void Calculator::showHelp() {
    QMessageBox * help = new QMessageBox(this);
    help->setText("Une simple calculatrice !");
    help->open();
}

void Calculator::left() {
    this->close();
}

void Calculator::buttonClick(int id) {
    if (controller->command((Controller::ButtonID) id)) {
        input->setText(controller->getText());
    }
}
